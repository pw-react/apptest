import * as React           from 'react';

class NoPage extends React.Component<{params: {id: number}}> {

    public render () {
        return (
            <div className="post-detail container-fluid">
                <div className="row mb-4">
                    <div className="col-12">
                        <h1>Page not found</h1>
                        <a className="btn btn-primary btn-block" href="/">Go Back</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default NoPage;
