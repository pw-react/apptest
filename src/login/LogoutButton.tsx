import * as React           from 'react';
import { browserHistory }   from 'react-router';
import SessionService       from '../login/Session.service';

class LogoutButton extends React.Component<{path?: string}> {
    public state: {pathname: string} = {
        pathname: '/login'
    };

    public constructor (props: any) {
        super(props);

        this.state = {
            pathname: props.path || this.state.pathname
        };

        this.logoutClick = this.logoutClick.bind(this);
    }

    public logoutClick (e: any) {
        SessionService.logout().then(() => {
            browserHistory.push(this.state.pathname);
        });
    }

    public render() {
        return (
            <button className="btn btn-primary btn-block" onClick={this.logoutClick}>Log Out</button>
        );
    }
}

export default LogoutButton;
