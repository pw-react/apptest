import * as React           from 'react';
import { browserHistory }   from 'react-router';
import Session              from './Session.service';

class LoginForm extends React.Component {
    public state: {
        dirty: boolean,
        user: {name: string, password: string},
        valid: {name: boolean, password: boolean}
    };

    public constructor (props: any) {
        super(props);

        this.state = {
            dirty: false,
            user: {name: '', password: ''},
            valid: {name: false, password: false}
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.setInputValue = this.setInputValue.bind(this);
        this.validForm = this.validForm.bind(this);
    }

    /**
     * Submit click.
     * @param {Event} e
     */
    public handleSubmit (e: any) {
        if (this.validForm()) {
            Session.login(this.state.user).then(() => {
                browserHistory.push('/');
            });
        }

        e.preventDefault();
    }

    /**
     * Valid user name;
     */
    public validateUserName (): boolean {
        if (!this.state.user.name || this.state.user.name.length < 5) {
            return false
        }

        return true;
    }

    /**
     * Valid user password.
     */
    public validateUserPassword (): boolean {
        if (!this.state.user.password || this.state.user.password.length < 8) {
            return false;
        }

        if (!this.state.user.password.match(/[0-9]/) || !this.state.user.password.match(/[a-z]/) || !this.state.user.password.match(/[A-Z]/)) {
            return false;
        }

        return true;
    }

    /**
     * Valid form.
     */
    public validForm (): boolean {
        const state = this.state;

        state.dirty = true;
        state.valid.name = this.validateUserName();
        state.valid.password = this.validateUserPassword();

        this.setState(state);

        return state.valid.name && state.valid.password;
    }

    /**
     * Set value to user object.
     * @param {Event} e
     */
    public setInputValue (e: any): void {
        const state = this.state;

        state.user[e.target.name] = e.target.value;

        this.setState(state);
    }

    public render () {
        const classesName = ['form-control'];
        const classesPassword = ['form-control'];

        if (this.state.dirty && !this.state.valid.name) {
            classesName.push('is-invalid');
        }

        if (this.state.dirty && !this.state.valid.password) {
            classesPassword.push('is-invalid');
        }

        return (
            <div className="login container-fluid">
                <div className="row">
                    <form className="offset-md-3 offset-lg-5 col-md-6 col-lg-2" onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="userName">User name</label>
                            <input
                                id="userName"
                                type="text"
                                className={classesName.join(' ')}
                                aria-describedby="userNameDesc"
                                placeholder="User Name"
                                name="name"
                                value={this.state.user.name}
                                onChange={this.setInputValue}/>
                            <small id="userNameDesc" className="form-text text-muted">User name to login</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="userPassword">Password</label>
                            <input
                                id="userPassword"
                                type="password"
                                className={classesPassword.join(' ')}
                                aria-describedby="userPasswordDesc"
                                placeholder="Password"
                                name="password"
                                value={this.state.user.password}
                                onChange={this.setInputValue} />
                            <small id="userPasswordDesc" className="form-text text-muted">User password</small>
                        </div>
                        <button type="submit" className="btn btn-primary">Log In</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default LoginForm
