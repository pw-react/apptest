import SessionStorageService    from '../SessionStorage.service';
import AuthService              from './Auth.service';
import User                     from './User';

/**
 * Session service.
 * @class
 * @type {SessionService}
 */
class SessionService {
    /**
     * Create Service.
     * @type {SessionService}
     */
    public static getInstance (): SessionService {
        if (!SessionService.sessionService) {
            SessionService.sessionService = new SessionService();
            SessionService.sessionService.restore();
        }

        return SessionService.sessionService;
    }

    private static sessionService: SessionService;

    private userObject: User;
    private sessionName: string = 'User';

    get session (): SessionService {
        return SessionService.getInstance();
    }

    get user (): User {
        return this.getUser();
    }

    /**
     * Create user session.
     * If user exist it will be overwrite.
     * @param {name: string, password: string} user
     *
     * @return {Promise}
     */
    public login (user: {name: string, password: string}): Promise<User | Error> {
        return AuthService.login(user).then((data: User) => {
            this.userObject = data;
            SessionStorageService.setItem(this.sessionName, JSON.stringify(this.userObject));

            return data;
        });
    }

    /**
     * Logout user.
     * @return {Promise}
     */
    public logout (): Promise<boolean | Error> {
        return AuthService.logout(this.userObject.token).then(() => {
            delete this.userObject;
            SessionStorageService.removeItem(this.sessionName);

            return true;
        });
    }

    /**
     * Return user session.
     * @return {User}
     */
    public getUser (): User {
        return this.userObject || null;
    }

    /**
     * Check if user login.
     * @return {boolean}
     */
    public isLogin (): boolean {
        return !!this.userObject;
    }

    /**
     * Restore user session.
     * @private
     */
    private restore (): void {
        const userSession = SessionStorageService.getItem(this.sessionName);

        if (userSession) {
            this.userObject = new User(JSON.parse(userSession));
        }
    }
}

export default SessionService.getInstance();
