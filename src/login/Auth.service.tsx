import User from './User';

class AuthService {
    /**
     * Get user from backend.
     * @param {name: string, password: string} user
     *
     * @return {Promise}
     */
    public static login (user: {name: string, password: string}): Promise<User | Error> {
        if (user.name && user.password) {
            return Promise.resolve(new User({name: user.name, token: 'any_token_string'}));
        }

        return Promise.reject(new Error('Wrong user or password'));
    }

    /**
     * Logout user from backend.
     * @param {string} token
     *
     * @return {Promise}
     */
    public static logout (token: string): Promise<string | Error> {
        if (token) {
            return Promise.resolve('OK');
        }

        return Promise.reject(new Error('Wrong token'));
    }
}

export default AuthService;
