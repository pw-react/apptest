import IUser        from './User.interface';

export default class User implements IUser {
    public id: number;
    public name: string;
    public token: string;

    constructor (data?: IUser) {
        Object.assign(this, data ? data : {});
    }
}
