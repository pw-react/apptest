class SessionStorageService {
    /**
     * Create instace.
     * @type {SessionStorageService}
     */
    public static getInstance (): SessionStorageService {
        if (!SessionStorageService.instance) {
            SessionStorageService.instance = new SessionStorageService();
        }

        return SessionStorageService.instance;
    }

    private static instance: SessionStorageService;

    private isSupport: boolean = false;

    private constructor () {
        if (typeof(Storage)!=="undefined") {
           this.isSupport = true;
        } else {
           this.isSupport = false;
           // tslint:disable-next-line:no-console
           console.error('No support for sessionStorage! The session will not be saved.');
        }
    }

    /**
     * Set session item.
     * @param {string} name
     * @param {string} value
     */
    public setItem (name: string, value: string): void {
        if (this.isSupport) {
            sessionStorage.setItem(name, value);
        }
    }

    /**
     * Get session item by name.
     * @param {string} name
     */
    public getItem (name: string): string | null {
        if (this.isSupport) {
            return sessionStorage.getItem(name);
        }

        return null;
    }

    /**
     * Remove session item by name.
     * @param {string} name
     */
    public removeItem (name: string): void {
        if (this.isSupport) {
            sessionStorage.removeItem(name);
        }
    }
}

export default SessionStorageService.getInstance();
