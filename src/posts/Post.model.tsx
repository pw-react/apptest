import IPost from './Post.interface';

enum userNames {
    'Antoni M.',
    'Gerald R.',
    'Kate C.',
    'Anne E.',
    'Rudolf S.',
    'Elise B.',
    'Getrude A.',
    'David P.',
    'Trans K.',
    'Magdalen L.'
}

export default class PostElement implements IPost {
    public id: number;
    public userId: number;
    public body: string;
    public title: string;

    get userName () {
        return userNames[this.userId];
    }

    constructor (data?: IPost) {
        Object.assign(this, data);
    }
}
