import PostElement      from './Post.model';

class PostsService {
    public static url: string = 'https://jsonplaceholder.typicode.com/posts';

    public static get (id?: number): Promise<any> {
        return new Promise((resolve: any, reject: any) => {
            const xhr = new XMLHttpRequest();

            xhr.open("GET", PostsService.url + (id ? '/' + id : ''));
            xhr.onload = () => {
                if(xhr.status === 200) {
                    resolve(PostsService.parse(xhr.responseText))
                } else {
                    reject(new Error('Data error'));
                }

            };
            xhr.onerror = () => reject(xhr.statusText);

            xhr.send();
        });
    }

    public static getInGroup (): Promise<PostElement[][]> {
        return PostsService.get().then((data: PostElement[]) => {
            const byUsersId: PostElement[][] = [];

            for (const item of data) {
                if (!byUsersId[item.userId]) {
                    byUsersId[item.userId] = [];
                }

                byUsersId[item.userId].push(item);
            }

            for (const key in byUsersId) {
                if (key) {
                    byUsersId[key].sort((a: PostElement, b: PostElement): number => {
                        if (a.id < b.id) {
                            return -1;
                        }
                        if (a.id > b.id) {
                            return 1;
                        }
                        return 0
                    });
                }
            }

            return byUsersId;
        });
    }

    private static parse (data: string): PostElement[] | PostElement {
        const jsonObj: any = JSON.parse(data);
        const r: PostElement[] | PostElement = [];

        if (jsonObj instanceof Array) {
            for (const item of jsonObj) {
                r.push(new PostElement(item));
            }

            return r;
        }

        return new PostElement(jsonObj);
    }
}

export default PostsService;


/*
r.sort((a: PostElement, b: PostElement): number => {
    if (a.id < b.id) {
        return -1;
    }
    if (a.id > b.id) {
        return 1;
    }
    return 0
})
 */
