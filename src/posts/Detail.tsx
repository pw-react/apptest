import * as React           from 'react';
import PostElement          from './Post.model';
import PostsService         from './Posts.service';

class PostDetail extends React.Component<{params: {id: number}}> {
    public state: {error: Error | string, post: PostElement} = {
        error: '',
        post: new PostElement()
    };

    constructor (props: any) {
        super(props);
    }

    public componentWillMount() {
        PostsService.get(this.props.params.id).then((data: PostElement) => {
            this.setState({post: data});
        }).catch(() => {
            this.setState({ error: 'Data not found' });
        });
    }

    public modalError () {
        if (this.state.error) {
            return (
                <div className="modal-open">
                    <div className="modal fade show modal-show" role="dialog" tabIndex={-1}>
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">Error</h5>
                                </div>
                                <div className="modal-body">
                                        {this.state.error}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal-overflow">&nbsp;</div>
                </div>
            );
        }

        return '';
    }

    public render () {
        return (
            <div className="post-detail container-fluid">
                <div className="row mb-4">
                    <div className="col-12">
                        <a className="btn btn-primary btn-block" href="/">Go Back</a>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <h3>Details</h3>
                        <p>UserId: {this.state.post.userId}</p>
                        <p>Id: {this.state.post.userName}</p>
                        <p>Title: {this.state.post.title}</p>
                        <p>Body: {this.state.post.body}</p>
                    </div>
                </div>
                {this.modalError()}
            </div>
        );
    }
}

export default PostDetail;
