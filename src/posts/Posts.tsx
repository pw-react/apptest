import * as React           from 'react';
import { browserHistory }   from 'react-router';
import LogoutButton         from '../login/LogoutButton';
import PostElement          from './Post.model';
import PostsService         from './Posts.service';

class Posts extends React.Component {
    public state: {posts: PostElement[][], search: string} = {posts: [], search: ''};
    private tempOrg: PostElement[][];

    public constructor (props: any) {
        super(props);

        this.setInputValue = this.setInputValue.bind(this);
    }

    public componentWillMount() {
        PostsService.getInGroup().then((data: PostElement[][]) => {
            let i: number = 1;

            this.tempOrg = data;

            for (const item of data) {
                const state = this.state;

                setTimeout(() => {
                    state.posts.push(item);

                    this.setState(state);
                }, i*1000);

                i++;
            }
        });
    }

    public cardClick (id: number, e: any) {
        browserHistory.push('/detail/' + id.toString());
        e.preventDefault();
    }

    public element (list: PostElement[]) {
        if( list ) {
            return list.map((post: PostElement) => {
                return (
                    <div className="card mb-4" key={post.id.toString()} onClick={this.cardClick.bind(this, post.id)}>
                        <div className="card-body">
                            <h5 className="card-title">UserId: {post.userId}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">Title: {post.title}</h6>
                        </div>
                    </div>
                );
            });
        }

        return '';
    }

    /**
     * Set value to user object.
     * @param {Event} e
     */
    public setInputValue (e: any): void {
        const state = this.state;

        state.search = e.target.value || '';

        this.search(state.search);

        this.setState(state);
    }

    public search (value: string) {
        const state = this.state;

        if (!value) {
            this.state.search = '';
            this.state.posts = this.tempOrg;
            this.setState(state);
            return;
        }

        const res: PostElement[][] = [];

        for (const group of this.tempOrg) {
            if (group) {
                for (const item of group) {
                    if (item.title.match(new RegExp(value)) || item.body.match(new RegExp(value))) {
                        if (!res[item.userId]) {
                            res[item.userId] = [];
                        }

                        res[item.userId].push(item);
                    }
                }
            }
        }

        state.posts = res;
        state.search = value;

        this.setState(state);
    }

    public render () {
        const listItems = this.state.posts.map((posts: PostElement[], index: number) => {
            if (posts) {
                return (
                    <div className="col-12 col-sm-6 col-md-4 mb-4" key={index}>
                        <div className="card">
                            <div className="card-body">
                                {this.element(posts)}
                            </div>
                        </div>
                    </div>
                );
            }

            return '';
        });
        return (
            <div className="posts container-fluid">
                <div className="row">
                    <div className="col-12 col-sm-6 col-md-4">
                        <LogoutButton path="/login" />
                    </div>
                    <div className="col-12 col-sm-6 col-md-8">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1">
                                    <i className="fas fa-search">&nbsp;</i>
                                </span>
                            </div>
                            <input
                                type="text"
                                className="form-control"
                                onChange={this.setInputValue}
                                placeholder="Search" />
                        </div>
                    </div>
                </div>
                <div className="row pt-4">
                    {listItems}
                </div>
            </div>
        );
    }
}

export default Posts;
