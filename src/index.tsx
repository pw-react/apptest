import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as React               from 'react';
import * as ReactDOM            from 'react-dom';
import { browserHistory, Redirect, Route, Router } from 'react-router';
import './index.css';
import Login                    from './login/Login';
import SessionService           from './login/Session.service';
import NoPage                   from './NoPage';
import PostDetail               from './posts/Detail';
import Posts                    from './posts/Posts';
import registerServiceWorker    from './registerServiceWorker';


function requireAuth (nextState: any, replaceState: any) {
    if (!SessionService.isLogin()) {
        replaceState({
            pathname: '/login',
            state: { nextPathname: nextState.location.pathname }
        });
    }
}


ReactDOM.render(
  (<Router history={browserHistory}>
    <Route path="/" component={Posts} onEnter={requireAuth}/>
    <Route path="/login" component={Login} />
    <Route path="/detail/:id" component={PostDetail} onEnter={requireAuth} />
    <Route path='/404' component={NoPage} />
    <Redirect from='*' to='/404' />
  </Router>),
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
